﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns
{
    class FactoryDemo
    {
        static void Main(string[] args)
        {
            ShapeFactory shapeFactory = new ShapeFactory();
            IShape circlesShape = shapeFactory.GetShape("circle");
            if (circlesShape != null)
            {
                circlesShape.Draw();
            }
            circlesShape = shapeFactory.GetShape("rectangle");
            if (circlesShape != null)
            {
                circlesShape.Draw();
            }

            Console.ReadKey();
        }
    }
}
