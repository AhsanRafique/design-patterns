﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns
{
    public interface IShape
    {
       void Draw();
    }

    public class Circle : IShape
    {
        public void Draw()
        {
            Console.WriteLine("Circle is drawn");
        }
    }
    public class Square : IShape
    {
        public void Draw()
        {
            Console.WriteLine("Square is drawn");
        }
    }
    public class Rectangle : IShape
    {
        public void Draw()
        {
            Console.WriteLine("Rectangle is drawn");
        }
    }

}
