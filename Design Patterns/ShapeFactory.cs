﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns
{
    public class ShapeFactory
    {
        private Dictionary<string, IShape> shapes = null;
        public ShapeFactory()
        {
            shapes = new Dictionary<string, IShape>();
            shapes.Add("circle", new Circle());
            shapes.Add("square", new Square());
            shapes.Add("rectangle", new Rectangle());
        }

        public IShape GetShape(string shapeName)
        {
            try
            {
                if (shapeName != null)
                {
                    return shapes[shapeName.ToLower()];
                }
                throw new Exception();
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}
